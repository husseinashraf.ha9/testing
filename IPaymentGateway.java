package Shopping;

public interface IPaymentGateway {
    boolean makePayment(int amount);
}
