package Shopping;

public interface Discounted {
    int getDiscount();
}
