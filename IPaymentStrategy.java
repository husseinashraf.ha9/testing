package Shopping;

public interface IPaymentStrategy {
    boolean pay(int amount);
}
